## 🚀 Quick start

Start using this starter pack:

```shell
gatsby new PWA_NAME https://gitlab.com/liyun-li/pwa-starter-pack
```

Pretty quick huh?

## Testing

```shell
npm run develop
```

## Components

🌱 Material UI

🌱 React DOM

🌱 React Redux

🌱 React Helmet

🌱 Rubik Font

🌱 TypeScript

🌱 gatsby-plugin-manifest

🌱 gatsby-plugin-offline

🌱 gatsby-plugin-react-helmet