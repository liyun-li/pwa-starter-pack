export const onServiceWorkerUpdateReady = () => {
  if (window.confirm(`Update available, update?`)) {
    window.location.reload()
  }
}