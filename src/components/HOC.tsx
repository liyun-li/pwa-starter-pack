import { Link, ListItem, ListItemIcon, ListItemText, makeStyles, SvgIconTypeMap, Theme, Typography, TypographyVariant } from '@material-ui/core'
import { OverridableComponent } from '@material-ui/core/OverridableComponent'
import { Link as Route } from 'gatsby'
import React from 'react'

const useStyles = makeStyles((theme: Theme) => ({
  externalLink: {
    textDecoration: 'none',
    color: theme.palette.text.secondary
  },
  justify: {
    textAlign: 'justify'
  }
}))

type AppRouteProps = {
  to: string
  children: any
  className?: string
  variant?: TypographyVariant
}

const AppRoute = (props: AppRouteProps) => {
  const { externalLink } = useStyles()
  return (
    <Typography variant={props.variant || 'body2'} align='center'>
      <Route to={props.to} className={props.className || externalLink} activeStyle={{ fontWeight: 'bolder' }}>
        {props.children}
      </Route>
    </Typography>
  )
}

type AppExternalLinkProps = {
  to: string
  children: any
  className?: string
  variant?: TypographyVariant
}

const AppExternalLink = (props: AppExternalLinkProps) => {
  const { externalLink } = useStyles()
  return (
    <Link href={props.to} className={props.className || externalLink} target='_blank'>
      <Typography variant={props.variant} align='center'>
        {props.children}
      </Typography>
    </Link>
  )
}

type ListItemRouteProps = {
  to: string
  text: string
  icon: OverridableComponent<SvgIconTypeMap<{}, "svg">>
}

const ListItemRoute = (props: ListItemRouteProps) => (
  <ListItem button component={Route} to={props.to} activeStyle={{ fontWeight: 'bolder' }}>
    <ListItemIcon><props.icon /></ListItemIcon>
    <ListItemText primary={props.text} />
  </ListItem>
)

type ListItemExternalLinkProps = {
  to: string
  text: string
  icon: OverridableComponent<SvgIconTypeMap<{}, "svg">>
}

const ListItemExternalLink = (props: ListItemExternalLinkProps) => (
  <ListItem button component={Link} href={props.to} target='_blank'>
    <ListItemIcon><props.icon /></ListItemIcon>
    <ListItemText secondary={props.text} />
  </ListItem>
)

export {
  AppExternalLink,
  AppRoute,
  ListItemRoute,
  ListItemExternalLink
}