import { useMediaQuery, useTheme } from '@material-ui/core'

/**
 * Guess whether user is on mobile based on viewport
 * @returns whether user is on mobile
 */
export const onMobile = (): boolean => {
  const theme = useTheme()
  const mobile: boolean = useMediaQuery(theme.breakpoints.down('sm'))
  return mobile
}