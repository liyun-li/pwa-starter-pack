import { configureStore, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux'

type AppState = {
  name: string
  data: string[][]
}

const initialState = {
  name: '',
  data: []
}

const app = createSlice({
  name: 'root',
  initialState,
  reducers: {
    setName: (state, action: PayloadAction<AppState['name']>) => ({
      ...state,
      name: action.payload
    }),
    setData: (state, action: PayloadAction<AppState['data']>) => ({
      ...state,
      data: action.payload
    })
  }
})

const reduxStore = configureStore({
  reducer: {
    app: app.reducer
  }
})

export type RootState = ReturnType<typeof reduxStore.getState>
export type AppDispatch = typeof reduxStore.dispatch

export const useAppDispatch = () => useDispatch<AppDispatch>()
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector

export const { setName, setData } = app.actions

export default reduxStore