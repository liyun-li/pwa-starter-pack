import { Grid, makeStyles, Typography } from '@material-ui/core'
import React from 'react'
import Layout from '../layouts/AppLayout'

const useStyles = makeStyles({
  container: {
    height: '75vh'
  }
})

const FourOhFour = () => {
  const { container } = useStyles()
  return (
    <Layout seo='404'>
      <Grid container justify='center' alignItems='center' className={container}>
        <Typography variant='h4'>Page Not Found :(</Typography>
      </Grid>
    </Layout>
  )
}

export default FourOhFour