import { Container, Typography } from '@material-ui/core'
import React from 'react'
import AppLayout from '../layouts/AppLayout'

const maxWidth = 'lg'

const App = () => {
  return (
    <AppLayout noMargin seo='Home'>
      <Container maxWidth={maxWidth}>
        <Typography variant='h1' style={{ textAlign: 'center' }}>
          PWA Starter Pack
        </Typography>
      </Container>
    </AppLayout>
  )
}

export default App