module.exports = {
  siteMetadata: {
    title: `PWA`,
    description: `Progressive Web Application`,
    author: `Liyun Li`
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Progressive Web Application`,
        short_name: `PWA`,
        start_url: `/`,
        display: `standalone`,
        icon: `${__dirname}/logo512.png`
      },
    },
    {
      resolve: `gatsby-plugin-offline`,
      options: {
        appendScript: require.resolve(`${__dirname}/src/sw.js`)
      },
    },
    `gatsby-plugin-material-ui`
  ],
}